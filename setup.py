from distutils.core import setup, Extension

module1 = Extension('yasdi',
                    include_dirs = [
                        '/usr/local/include/yasdi/protocol',
                        '/usr/local/include/yasdi/os',
                        '/usr/local/include/yasdi/core',
                        '/usr/local/include/yasdi/smalib',
                        '/usr/local/include/yasdi/include',
                        '/usr/local/include/yasdi/projects/generic-cmake/incprj',
                        '/usr/local/include/yasdi/libs'
                        ],
                    libraries = ['yasdimaster','yasdi'],
                    sources = ['yasdimodule.c'])

setup (name = 'Yasdi',
       version = '1.0',
       description = 'The YASDI library for communicating with SMA inverters',
       ext_modules = [module1])
