#include <Python.h>

#include "libyasdimaster.h"
#include <assert.h>

static PyObject *
yasdi_initialize(PyObject *self, PyObject *args)
{
    const char *inifile;

    if (!PyArg_ParseTuple(args, "s", &inifile))
        return NULL;

    unsigned int drivercount;
    yasdiMasterInitialize(inifile, &drivercount);

    unsigned int * driver_ids = malloc(drivercount*sizeof(unsigned int));

    unsigned int dc2 = yasdiMasterGetDriver(driver_ids, drivercount);
    assert (dc2==drivercount);

    PyObject* py_driver_ids = PyList_New(drivercount);
    int i;
    for(i = 0; i < drivercount; ++i)
    {
        yasdiMasterSetDriverOnline(driver_ids[i]);
        PyList_SET_ITEM(py_driver_ids, i, Py_BuildValue("I", driver_ids[i]));
    }

    free(driver_ids);
    return py_driver_ids;
}

static PyObject *
yasdi_detect(PyObject *self, PyObject *args)
{
    int n_devices;

    if (!PyArg_ParseTuple(args, "i", &n_devices))
        return NULL;

    int status = yasdiDoMasterCmdEx("detect",n_devices,0,0);
    if (status)
    {
        PyErr_SetString(PyExc_IOError, "Could not detect all devices");
        return NULL;
    }
    
    // get device handles

    unsigned int * device_handles = malloc(n_devices*sizeof(unsigned int));
    
    uint ndevicehandle = GetDeviceHandles(device_handles, n_devices);
    assert (ndevicehandle == n_device);

    PyObject* py_device_handles = PyList_New(n_devices);
    int i;
    for(i = 0; i < n_devices; ++i)
    {
        PyList_SET_ITEM(py_device_handles, i, Py_BuildValue("I", device_handles[i]));
    }

    free(device_handles);
    return py_device_handles;
}

static PyObject *
yasdi_getdevicesn(PyObject *self, PyObject *args)
{
    unsigned int device_handle;
    if (!PyArg_ParseTuple(args, "I", &device_handle))
        return NULL;

    unsigned int device_sn;
    int status = GetDeviceSN(device_handle, &device_sn);
    if (status)
    {
        PyErr_SetString(PyExc_IOError, "No such device");
        return NULL;
    }

    return Py_BuildValue("I", device_sn);
}

static PyObject *
yasdi_getchannelhandle(PyObject *self, PyObject *args)
{
    unsigned int device_handle;
    char * channel_name;

    if (!PyArg_ParseTuple(args, "Is", &device_handle, &channel_name))
        return NULL;

    unsigned int channel_handle = FindChannelName(device_handle, 
                                                  channel_name);
    if (!channel_handle)
    {
        PyErr_SetString(PyExc_IOError, "No such channel.");
        return NULL;
    }

    return Py_BuildValue("I", channel_handle);
}

static PyObject *
yasdi_getchannelname(PyObject *self, PyObject *args)
{
    unsigned int channel_handle;

    if (!PyArg_ParseTuple(args, "I", &channel_handle))
        return NULL;

    char buf[100];
    int status = GetChannelName(channel_handle, buf, 100);
    if (status)
    {
        PyErr_SetString(PyExc_IOError, "No such channel.");
        return NULL;
    }

    return Py_BuildValue("s", buf);
}

static PyObject *
yasdi_getchannelunit(PyObject *self, PyObject *args)
{
    unsigned int channel_handle;

    if (!PyArg_ParseTuple(args, "I", &channel_handle))
        return NULL;

    char buf[100];
    int status = GetChannelUnit(channel_handle, buf, 100);
    if (status)
    {
        PyErr_SetString(PyExc_IOError, "No such channel.");
        return NULL;
    }

    return Py_BuildValue("s", buf);
}

static PyObject *
yasdi_getchannelvalue(PyObject *self, PyObject *args)
{
    unsigned int channel_handle;
    unsigned int device_handle;

    if (!PyArg_ParseTuple(args, "II", &channel_handle, &device_handle))
        return NULL;

    char buf[100];
    double val;
    int status = GetChannelValue(channel_handle, device_handle, &val, buf, 100, 0);
    if (status == -1)
    {
        PyErr_SetString(PyExc_IOError, "No such channel.");
        return NULL;
    }
    else if (status)
    {
        PyErr_SetString(PyExc_IOError, "Error getting channel value.");
        return NULL;
    }

    return Py_BuildValue("ds", val, buf);
}

static PyObject *
yasdi_shutdown(PyObject *self, PyObject *args)
{
    yasdiMasterShutdown();
    
    Py_RETURN_NONE;
}

static PyMethodDef YasdiMethods[] = {
    {"initialize",  yasdi_initialize, METH_VARARGS, "Initialize the YASDI library."},
    {"detect",  yasdi_detect, METH_VARARGS, "Detect SMA devices and return handles."},
    {"get_device_sn",  yasdi_getdevicesn, METH_VARARGS, "Get device SN."},
    {"get_channel_handle",  yasdi_getchannelhandle, METH_VARARGS, "Get channel handle."},
    {"get_channel_name",  yasdi_getchannelname, METH_VARARGS, "Get channel name."},
    {"get_channel_unit",  yasdi_getchannelunit, METH_VARARGS, "Get channel unit."},
    {"get_channel_value",  yasdi_getchannelvalue, METH_VARARGS, "Get channel value."},
    {"shutdown",  yasdi_shutdown, METH_NOARGS, "Shut down the YASDI library."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

PyMODINIT_FUNC
inityasdi(void)
{
    (void) Py_InitModule("yasdi", YasdiMethods);
}
